<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="generator" content="pandoc">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
  <title></title>
  <style type="text/css">code{white-space: pre;}</style>
  <style type="text/css">
div.sourceCode { overflow-x: auto; }
table.sourceCode, tr.sourceCode, td.lineNumbers, td.sourceCode {
  margin: 0; padding: 0; vertical-align: baseline; border: none; }
table.sourceCode { width: 100%; line-height: 100%; }
td.lineNumbers { text-align: right; padding-right: 4px; padding-left: 4px; color: #aaaaaa; border-right: 1px solid #aaaaaa; }
td.sourceCode { padding-left: 5px; }
code > span.kw { color: #007020; font-weight: bold; } /* Keyword */
code > span.dt { color: #902000; } /* DataType */
code > span.dv { color: #40a070; } /* DecVal */
code > span.bn { color: #40a070; } /* BaseN */
code > span.fl { color: #40a070; } /* Float */
code > span.ch { color: #4070a0; } /* Char */
code > span.st { color: #4070a0; } /* String */
code > span.co { color: #60a0b0; font-style: italic; } /* Comment */
code > span.ot { color: #007020; } /* Other */
code > span.al { color: #ff0000; font-weight: bold; } /* Alert */
code > span.fu { color: #06287e; } /* Function */
code > span.er { color: #ff0000; font-weight: bold; } /* Error */
code > span.wa { color: #60a0b0; font-weight: bold; font-style: italic; } /* Warning */
code > span.cn { color: #880000; } /* Constant */
code > span.sc { color: #4070a0; } /* SpecialChar */
code > span.vs { color: #4070a0; } /* VerbatimString */
code > span.ss { color: #bb6688; } /* SpecialString */
code > span.va { color: #19177c; } /* Variable */
code > span.cf { color: #007020; font-weight: bold; } /* ControlFlow */
code > span.op { color: #666666; } /* Operator */
code > span.pp { color: #bc7a00; } /* Preprocessor */
code > span.at { color: #7d9029; } /* Attribute */
code > span.do { color: #ba2121; font-style: italic; } /* Documentation */
code > span.an { color: #60a0b0; font-weight: bold; font-style: italic; } /* Annotation */
code > span.cv { color: #60a0b0; font-weight: bold; font-style: italic; } /* CommentVar */
code > span.in { color: #60a0b0; font-weight: bold; font-style: italic; } /* Information */
  </style>
  <style>
      @import url('https://fonts.googleapis.com/css?family=Fira+Mono|Oswald|Source+Sans+Pro');

body {
    background-color: #efefef;
    margin: 20px;
    width: 100;
}

h1, h2, h3, h4, h5, h6 {
    font-family: 'Oswald', sans-serif;
    color: #ff383f;
}

code {
    font-family: 'Fira Mono', monospace;
    font-size: 14; 
}

p, ol, ul {
    font-family: 'Source Sans Pro', sans-serif;
    color: #18122e
}


/* Solarized Light */
pre {
    background-color: #FDF6E3
}

/* KeyWordTok */
.sourceCode .kw { color: #268BD2; }
/* DataTypeTok */
.sourceCode .dt { color: #268BD2; }

/* DecValTok (decimal value), BaseNTok, FloatTok */
.sourceCode .dv, .sourceCode .bn, .sourceCode .fl { color: #D33682; }
/* CharTok */
.sourceCode .ch { color: #DC322F; }
/* StringTok */
.sourceCode .st { color: #2AA198; }
/* CommentTok */
.sourceCode .co { color: #93A1A1; }
/* OtherTok */
.sourceCode .ot { color: #A57800; }
/* AlertTok */
.sourceCode .al { color: #CB4B16; font-weight: bold; }
/* FunctionTok */
.sourceCode .fu { color: #268BD2; }
/* RegionMarkerTok */
.sourceCode .re {color: #268BD2; }
/* ErrorTok */
.sourceCode .er { color: #D30102; font-weight: bold; }
      </style>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="behavior-tree">Behavior Tree</h1>
<h3 id="problem">Problem</h3>
<p>You need to implement some complex behavior but you're running into one of the main limitations of finite state machines: <strong>hard-coded transitions</strong>. In an FSM the states internally contain the logic for deciding when and which state to transition to. This leads to three problems:</p>
<ol type="1">
<li><p><em>Poor Scalabilty</em>: When you add or remove a state to an FSM you often have to update a lot of other states to handle the transitions. For example, say you have a simple creature behavior where the creature can either be <code>Searching</code>, <code>Eating</code>, or <code>Sleeping</code>. You decide to add enemies to the game, so the creature now needs a <code>Fleeing</code> state when an enemy is near. To implement that the <code>Searching</code> and <code>Eating</code> states need to add a check for enemy proximity and transition to the <code>Fleeing</code> state. If you ever decide to change the creature so that it attacks rather than flees, you'll need to go to all those states and change the logic again. For small FSMs that's not a big deal, but once you have complex behaviors with 10, 20 or more states that process can become very tedious and bug prone.</p></li>
<li><p><em>Poor Reusability</em>: Transitions are a form of dependency. If State X can transition to Y or Z it means that State X is dependent on Y or Z being present in any FSM it's placed in. Because of those dependencies it's hard to create generic or modular states that can be easily reused in different FSMs.</p></li>
<li><p><em>Programmer Dependent</em>: Since you can't easily make generic/modular states that can be plugged together, you'll generally need a programmer to tweak your FSMs (i.e. it's hard to make a visual or other kind of designer-friendly editor for something that needs to be expressed in arbitrary logic). AI behaviors impact the player very heavily and really define the gameplay, therefore it's something you want to tweak and expand a lot. It's going to be hard for designers to do much tweakivng if they need to go through a programmer for all of it.</p></li>
</ol>
<h3 id="solution-behavior-trees">Solution: Behavior Trees</h3>
<p>Behavior Trees<sup id="a1"><a href="#f1">1</a></sup> (BTs) are a refinement on FSMs that were first <a href="http://www.gamasutra.com/view/feature/130663/gdc_2005_proceeding_handling_.php">developed by Bungie in <em>Halo 2</em></a>, but have since supplanted state machines as the dominant tool for AI in games (and have also found success in robotics).</p>
<p>Both BTs and FSMs are types of <a href="https://en.wikipedia.org/wiki/Graph_theory">graphs</a> - i.e. they can be thought of as a network of 'nodes' or 'vertices' connected to each other by 'edges'.</p>
<p>In FSMs the nodes are very 'heavy'. They encapsulate actions (e.g. 'Sleep', 'Attack', etc.) but also the logic for deciding between the different states (i.e. the transitions), which leads to the problems discussed above.</p>
<p>In BTs nodes are much 'lighter' - all they need to do is report their status when updated (i.e. success or failure)<sup id="a2"><a href="#f2">2</a></sup>. The tasks of deciding what to do and then actually doing it can be split up into three different classes of nodes:</p>
<ul>
<li><p><em>Composite Nodes</em>: These are the 'branches' of the tree and are defined by having one or more children. These represent the decision points in the tree (e.g. &quot;Should I attack or flee&quot;, &quot;Should I eat, sleep, or frolick?&quot;), and determine the flow of execution through the tree (i.e. which branches to go down and how far). The most common composite nodes are:</p>
<ul>
<li><p><em>Selector</em>: These nodes are used to select one of their children. They return success as soon as one of their children is successful and return failure when all of their fail. They are usually represented graphically with a <code>?</code>.</p></li>
<li><p><em>Sequence</em>: These nodes are used to implement 'checklists' (e.g. If I have ammo, and I'm in range then attack). They return success when all their children are successfull and return failure as soon as one of their children fails. They are usually represented graphically with a <code>-&gt;</code></p></li>
<li><p><em>Decorator</em>: These are special cases of composites that have a single child, and are used to modify the return value of another node (e.g. imagine you have a node that lets you know if a player is range, you might use a negation decorator if you want to know when a player is <em>NOT</em> in range). They are usually represented graphically with a diamond.</p></li>
</ul></li>
<li><p><em>Action Nodes</em>: These are nodes at the outer edges of the tree (i.e. the 'leaves'). These are the nodes you end up at the end of updating a tree and where you'll put the code to actually affect world in some way (e.g. move the character towards the chest, emit a new enemy, etc.). They are usually represented graphically with a box.</p></li>
<li><p><em>Condition Nodes</em>: Conditions are also 'leaf' nodes but unlike actions they don't change the world in any way. They're one of the tools used by Composite nodes to decide where to go next. They are usually represented graphically with an ellipse.</p></li>
</ul>
<p>So using those nodes, let's see what a possible toddler AI might look like (in pseudo-code and in diagram format):</p>
<h4 id="pseudo-code">Pseudo code</h4>
<div class="sourceCode"><pre class="sourceCode cs"><code class="sourceCode cs"><span class="co">// Select behavior</span>
<span class="fu">selector</span>(
    <span class="co">// Eat Treat</span>
    <span class="fu">sequence</span>(
        <span class="fu">Not</span>(IsFull?)
        <span class="co">// Get Treat</span>
        <span class="fu">sequence</span>(
            IsGrownupNear?
            GetTreat
        )
        EatTreat
    ),
    <span class="co">// Watch video</span>
    <span class="fu">sequence</span>(
        <span class="fu">Not</span>(CrankyFromTooManyVideos?)
        HasIPad?
        WatchVideo
    ),
    <span class="co">// Default</span>
    <span class="fu">ThrowConniptionFit</span>()
);</code></pre></div>
<h4 id="diagram">Diagram</h4>
<p><<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="1248.61" height="496.82" viewBox="0 0 1248.61 496.82"><defs><style>.cls-1{fill:#fff;}.cls-1,.cls-14,.cls-3,.cls-5{stroke:#231f20;}.cls-1,.cls-14,.cls-23,.cls-3,.cls-5{stroke-miterlimit:10;}.cls-2{font-size:45.98px;}.cls-2,.cls-6{font-family:Futura-Medium, Futura;}.cls-14,.cls-22,.cls-23,.cls-3{fill:none;}.cls-3{stroke-width:6px;}.cls-4{fill:#231f20;}.cls-5{fill:#c4c4c4;}.cls-6{font-size:21px;}.cls-7{letter-spacing:-0.01em;}.cls-8{letter-spacing:-0.02em;}.cls-9{letter-spacing:0.01em;}.cls-10{letter-spacing:-0.03em;}.cls-11{letter-spacing:-0.01em;}.cls-12{letter-spacing:0.01em;}.cls-13{letter-spacing:0.01em;}.cls-15{letter-spacing:-0.02em;}.cls-16{letter-spacing:-0.03em;}.cls-17{letter-spacing:-0.06em;}.cls-18{letter-spacing:0.03em;}.cls-19{letter-spacing:0.01em;}.cls-20{letter-spacing:0em;}.cls-21{letter-spacing:-0.08em;}.cls-23{stroke:#000;}.cls-24{letter-spacing:0.02em;}</style></defs><title>Toddler</title><rect class="cls-1" x="618.19" y="32.57" width="68.7" height="68.7"/><text class="cls-2" transform="translate(640.91 85.07)">?</text><rect class="cls-1" x="263.46" y="156.55" width="68.7" height="68.7"/><line class="cls-3" x1="283.29" y1="192.39" x2="309.48" y2="192.39"/><polygon class="cls-4" points="297.52 202.56 307.7 192.39 297.52 182.22 306.16 182.22 316.33 192.39 306.16 202.56 297.52 202.56"/><ellipse class="cls-5" cx="243.11" cy="443.59" rx="67.39" ry="47.61"/><text class="cls-6" transform="translate(200.98 435.43)">g<tspan class="cls-7" x="12.57" y="0">r</tspan><tspan class="cls-8" x="20.24" y="0">o</tspan><tspan x="32.46" y="0">wnup</tspan><tspan x="15.21" y="25.2">near?</tspan></text><ellipse class="cls-5" cx="67.89" cy="447.07" rx="67.39" ry="47.61"/><text class="cls-6" transform="translate(48.08 456.44)">full?</text><rect class="cls-1" x="208.76" y="291.51" width="68.7" height="68.7"/><line class="cls-3" x1="228.59" y1="327.35" x2="254.77" y2="327.35"/><polygon class="cls-4" points="242.82 337.52 253 327.35 242.82 317.18 251.46 317.18 261.63 327.35 251.46 337.52 242.82 337.52"/><rect class="cls-5" x="337.89" y="396.2" width="150.43" height="94.78"/><text class="cls-6" transform="translate(368.71 453.23)">G<tspan class="cls-9" x="17.69" y="0">e</tspan><tspan x="29.49" y="0">t </tspan><tspan class="cls-10" x="41.71" y="0">T</tspan><tspan class="cls-11" x="51.13" y="0">r</tspan><tspan x="58.84" y="0">eat</tspan></text><rect class="cls-1" x="-594.39" y="539.23" width="68.7" height="68.7" transform="translate(869.48 316.29) rotate(45)"/><text class="cls-6" transform="translate(48.46 332.74)"><tspan class="cls-12">N</tspan><tspan class="cls-13" x="17.87" y="0">o</tspan><tspan x="30.62" y="0">t</tspan></text><line class="cls-1" x1="67.28" y1="374.22" x2="67.28" y2="399.46"/><line class="cls-14" x1="243.11" y1="360.2" x2="243.11" y2="395.98"/><line class="cls-14" x1="413.11" y1="395.98" x2="243.11" y2="360.2"/><rect class="cls-5" x="395.15" y="278.46" width="150.43" height="94.78"/><text class="cls-6" transform="translate(428.48 335.49)">Eat <tspan class="cls-10" x="36.69" y="0">T</tspan><tspan class="cls-11" x="46.1" y="0">r</tspan><tspan x="53.81" y="0">eat</tspan></text><line class="cls-14" x1="67.89" y1="277.28" x2="290.54" y2="225.24"/><line class="cls-14" x1="243.11" y1="291.51" x2="290.54" y2="225.24"/><line class="cls-14" x1="459.63" y1="278.46" x2="297.81" y2="225.24"/><rect class="cls-1" x="797.3" y="169.17" width="68.7" height="68.7"/><line class="cls-3" x1="817.13" y1="205.01" x2="843.31" y2="205.01"/><polygon class="cls-4" points="831.36 215.19 841.54 205.01 831.36 194.84 840 194.84 850.17 205.01 840 215.19 831.36 215.19"/><ellipse class="cls-5" cx="648.76" cy="448.71" rx="67.39" ry="47.61"/><text class="cls-6" transform="translate(641.4 440.54)">Is<tspan x="-31.77" y="25.2">C</tspan><tspan class="cls-11" x="-17.06" y="25.2">r</tspan><tspan x="-9.35" y="25.2">an</tspan><tspan class="cls-15" x="14.76" y="25.2">k</tspan><tspan x="24.8" y="25.2">y?</tspan></text><rect class="cls-1" x="-13.52" y="540.87" width="68.7" height="68.7" transform="translate(1040.77 -93.97) rotate(45)"/><text class="cls-6" transform="translate(629.33 334.38)"><tspan class="cls-12">N</tspan><tspan class="cls-13" x="17.87" y="0">o</tspan><tspan x="30.62" y="0">t</tspan></text><ellipse class="cls-5" cx="808.3" cy="328.99" rx="67.39" ry="47.61"/><text class="cls-6" transform="translate(790.04 320.83)">Has<tspan x="-7.47" y="25.2">i</tspan><tspan class="cls-16" x="-2.25" y="25.2">P</tspan><tspan x="8.53" y="25.2">ad?</tspan></text><rect class="cls-5" x="902.07" y="280.1" width="150.43" height="94.78"/><text class="cls-6" transform="translate(915.09 337.13)"><tspan class="cls-17">W</tspan><tspan x="21.03" y="0">at</tspan><tspan class="cls-18" x="39.36" y="0">c</tspan><tspan x="49.92" y="0">h </tspan><tspan class="cls-19" x="67.91" y="0">V</tspan><tspan x="82.32" y="0">ideo</tspan></text><line class="cls-14" x1="648.76" y1="278.92" x2="831.65" y2="237.86"/><line class="cls-14" x1="808.3" y1="281.38" x2="831.65" y2="237.86"/><line class="cls-14" x1="971.8" y1="281.38" x2="831.65" y2="237.86"/><line class="cls-14" x1="648.76" y1="401.1" x2="648.76" y2="376.07"/><rect class="cls-5" x="1097.67" y="157.62" width="150.43" height="94.78"/><text class="cls-6" transform="translate(1143.99 188.91)">Th<tspan class="cls-7" x="21.65" y="0">r</tspan><tspan class="cls-8" x="29.32" y="0">o</tspan><tspan x="41.54" y="0">w</tspan><tspan x="-26.09" y="25.2">Conni</tspan><tspan class="cls-9" x="29.51" y="25.2">p</tspan><tspan x="42.29" y="25.2">tion </tspan><tspan class="cls-20" x="18.89" y="50.4">F</tspan><tspan x="29.61" y="50.4">i</tspan><tspan class="cls-21" x="34.84" y="50.4">t</tspan></text><line class="cls-22" x1="618.19" y1="101.27" x2="633.54" y2="101.27"/><line class="cls-23" x1="297.81" y1="157.62" x2="648.76" y2="101.27"/><line class="cls-23" x1="831.65" y1="169.17" x2="648.76" y2="101.27"/><line class="cls-23" x1="648.76" y1="101.27" x2="1165.72" y2="157.62"/><text class="cls-6" transform="translate(262.1 135.22)">Eat <tspan class="cls-10" x="36.69" y="0">T</tspan><tspan class="cls-11" x="46.1" y="0">r</tspan><tspan x="53.81" y="0">eat</tspan></text><text class="cls-6" transform="translate(780.15 157.62)"><tspan class="cls-17">W</tspan><tspan x="21.03" y="0">at</tspan><tspan class="cls-18" x="39.36" y="0">c</tspan><tspan x="49.92" y="0">h </tspan><tspan class="cls-19" x="67.91" y="0">V</tspan><tspan x="82.32" y="0">ideo</tspan></text><text class="cls-6" transform="translate(1138.68 144.1)">De<tspan class="cls-24" x="26.87" y="0">f</tspan><tspan x="33.42" y="0">ault</tspan></text><text class="cls-6" transform="translate(578.78 18.49)">Select Beh<tspan class="cls-9" x="98.49" y="0">a</tspan><tspan x="111.27" y="0">vior</tspan></text><text class="cls-6" transform="translate(205.96 281.38)">G<tspan class="cls-9" x="17.69" y="0">e</tspan><tspan x="29.49" y="0">t </tspan><tspan class="cls-10" x="41.71" y="0">T</tspan><tspan class="cls-11" x="51.13" y="0">r</tspan><tspan x="58.84" y="0">eat</tspan></text></svg></img></p>
<h4 id="execution">Execution</h4>
<p>When you update a BT you start at the root of the tree and recursively update the children based on th elogic embedded in the tree. Using our toddler example above here's what an update might look like: 
<ol start="1" type="1">
<li><p>We start with the root object (the selector at the top)</p></li>
<li><p>Since its a selector it will go through all the nodes until it finds a successful one or all of them fail. Trees typically are evaluated from left to right so we'll start with the <code>Eat Treat</code> branch.</p></li>
<li><p><code>Eat Treat</code> is defined as a sequence, so it's effectively a checklist that will go through all the conditions until it reaches the end. In this case it first checks...</p></li>
<li><p>Whether it's full. We'll say no. Since it's wrapped in a <code>Not</code> decorator that inverts the status of its child this will evaluate to true moving the sequence to the next step.</p></li>
<li><p>Since we're not full let's see about getting a treat. For this we enter into another sequence and check...</p></li>
<li><p>If <code>Is Grownup Near</code>. We'll say no. This causes the <code>Get Treat</code> sequence to return failure, which in turn causes the <code>Eat Treat</code> sequence to fail bringing us all the way back to the root. Since a treat is not an option let's try the next option...</p></li>
<li><p>The <code>Watch Video</code> branch is another sequence...</p></li>
<li><p>We check wether we're too <code>Cranky From Too Many Videos</code>. We're not and since this is wrapped in a <code>Not</code> decorator, this evaluates to success. On to the next item...</p></li>
<li><p><code>Has iPad?</code> The answer is yes so we go to the next node...</p></li>
<li><p><code>Watch video</code>! We've successfully gotten to an action. We perform the action it returns success (nothing happened internally to the action like the iPad breaking, or the network going down that prevents success). We're at the end of the sequence, so we return success to the parent (the root in this case).</p></li>
<li><p>The root is a selector which means it returns success as soon as one of its children succeed, so we're done. If the <code>Watch Video</code> action had failed we would have moved on to the last branch <code>Throw Conniption Fit</code> which has no conditions and would have ended the process of evaluating the tree.</p></li>
</ol>
<h3 id="implementing-a-behavior-tree">Implementing A Behavior Tree</h3>
<p>There are many different approaches to implementing BTs. The one below is a stateless/functional one. This is the original style of implementation for behavior trees and makes for very simple code with some runtime advantages.</p>
<p>For example, because it's stateless you can use the same tree for as many entities as you want. Have a horde of goblins that need to be updated with some complex logic? They can all use the same tree, saving a lot of memory and speeding things up by lowering the pressure on the data cache.</p>
<p>The downside of stateless implementations is that they are entirely reactive and have a hard time representing behaviors that exhibit long term planning or need state (just like you would have a hard time engaging in planning if you had no short term memory). Techniques exist for addressing these shortcomings but they entail more complex implementations that aren't ideal as your first foray into behavior trees.</p>
<div class="sourceCode"><pre class="sourceCode cs"><code class="sourceCode cs"><span class="kw">using</span> System;

<span class="kw">namespace</span> BehaviorTree
{     
    <span class="co">// The basic building block of a behavior tree is a node. The only requirement for a node is that</span>
    <span class="co">// it report success or failure when it is updated. It takes a context variable so that leaf nodes</span>
    <span class="co">// know who or what to operate on.    </span>
    <span class="kw">public</span> <span class="kw">abstract</span> <span class="kw">class</span> Node&lt;T&gt;
    {
        <span class="kw">public</span> <span class="kw">abstract</span> <span class="dt">bool</span> <span class="fu">Update</span>(T context);
    }

    <span class="co">// Trees themselves are just a special kind of node that contain another node as a &#39;root&#39;. The root</span>
    <span class="co">// is where the execution of the tree always start.</span>
    <span class="co">// Because trees are regular nodes they can be added to other trees as sub-trees.</span>
    <span class="kw">public</span> <span class="kw">class</span> Tree&lt;T&gt; : Node&lt;T&gt;
    {
        <span class="kw">private</span> <span class="kw">readonly</span> Node&lt;T&gt; _root;

        <span class="kw">public</span> <span class="fu">Tree</span>(Node&lt;T&gt; root) {
            _root = root;
        }

        <span class="kw">public</span> <span class="kw">override</span> <span class="dt">bool</span> <span class="fu">Update</span>(T context)
        {
            <span class="kw">return</span> _root.<span class="fu">Update</span>(context);
        }
    }

    <span class="co">////////////////////////////////////////////////////////////////////////////////////////////////////////////////////</span>
    <span class="co">// NODE TYPES</span>
    <span class="co">////////////////////////////////////////////////////////////////////////////////////////////////////////////////////</span>


    <span class="co">////////////////////////////////////////////////////////////////////////////////////////////////////////////////////</span>
    <span class="co">// OUTER, LEAF OR &#39;ACTION&#39; NODES</span>
    <span class="co">////////////////////////////////////////////////////////////////////////////////////////////////////////////////////</span>

    <span class="co">// Outer nodes are the &#39;leaves&#39; of the tree and are where the action is. Literally.</span>
    <span class="co">// The outer nodes is where the tree reaches a decision of what to do and actually</span>
    <span class="co">// performs some aspect of the behavior (e.g. attacks an enemy, sounds an alarm, etc.)</span>
    <span class="co">// Because it&#39;s where the game-specific actions you&#39;ll most likely make subclasses</span>
    <span class="co">// for these but here is a basic version using delegates that allows you to write them &#39;in-line&#39;</span>
    <span class="kw">public</span> <span class="kw">class</span> Do&lt;T&gt; : Node&lt;T&gt;
    {
        <span class="kw">public</span> <span class="kw">delegate</span> <span class="dt">bool</span> <span class="fu">NodeAction</span>(T context);

        <span class="kw">private</span> <span class="kw">readonly</span> NodeAction _action;

        <span class="kw">public</span> <span class="fu">Do</span>(NodeAction action)
        {
            _action = action;
        }

        <span class="kw">public</span> <span class="kw">override</span> <span class="dt">bool</span> <span class="fu">Update</span>(T context)
        {
            <span class="kw">return</span> _<span class="fu">action</span>(context);
        }
    }

    <span class="co">// CONDITION</span>
    <span class="co">// Conditions are just leaf nodes that test something. Similarly you can create subclasses for these</span>
    <span class="co">// or else use the version below to write them inline.</span>
    <span class="kw">public</span> <span class="kw">class</span> Condition&lt;T&gt; : Node&lt;T&gt;
    {
        <span class="kw">private</span> <span class="kw">readonly</span> Predicate&lt;T&gt; _condition;

        <span class="kw">public</span> <span class="fu">Condition</span>(Predicate&lt;T&gt; condition)
        {
            _condition = condition;
        }

        <span class="kw">public</span> <span class="kw">override</span> <span class="dt">bool</span> <span class="fu">Update</span>(T context)
        {
            <span class="kw">return</span> _<span class="fu">condition</span>(context);
        }
    }

    <span class="co">////////////////////////////////////////////////////////////////////////////////////////////////////////////////////</span>
    <span class="co">// INNER, COMPOSITE OR &#39;DECISION&#39; NODES</span>
    <span class="co">////////////////////////////////////////////////////////////////////////////////////////////////////////////////////</span>

    <span class="co">// What defines an inner node is that it has a set of &#39;child&#39; or &#39;branch&#39; nodes.</span>
    <span class="co">// These are the nodes that define the structure and logic of three but they</span>
    <span class="co">// won&#39;t actually *do* anything</span>
    <span class="kw">public</span> <span class="kw">abstract</span> <span class="kw">class</span> BranchNode&lt;T&gt; : Node&lt;T&gt;
    {
        <span class="kw">protected</span> Node&lt;T&gt;[] Children { <span class="kw">get</span>; <span class="kw">private</span> <span class="kw">set</span>; }

        <span class="kw">protected</span> <span class="fu">BranchNode</span>(<span class="kw">params</span> Node&lt;T&gt;[] children)
        {
            Children = children;
        }
    }

    <span class="co">// SELECTOR</span>
    <span class="co">// Succeeds when ONE of its children succeeded</span>
    <span class="co">// Fails when ALL of its children failed</span>
    <span class="co">// Used to select from a series of ranked options (e.g. Try to cast a spell, if not close for a melee attack)</span>
    <span class="kw">public</span> <span class="kw">class</span> Selector&lt;T&gt; : BranchNode&lt;T&gt;
    {
        <span class="kw">public</span> <span class="fu">Selector</span>(<span class="kw">params</span> Node&lt;T&gt;[] children) : <span class="kw">base</span>(children) {}

        <span class="kw">public</span> <span class="kw">override</span> <span class="dt">bool</span> <span class="fu">Update</span>(T context)
        {
            <span class="kw">foreach</span> (var child <span class="kw">in</span> Children)
            {
                <span class="kw">if</span> (child.<span class="fu">Update</span>(context)) <span class="kw">return</span> <span class="kw">true</span>;
            }
            <span class="kw">return</span> <span class="kw">false</span>;
        }
    }

    <span class="co">// SEQUENCE</span>
    <span class="co">// Succeeds when ALL of its children succeeded</span>
    <span class="co">// Fails when ONE of its children failed</span>
    <span class="co">// Used for things like checklists (e.g. If there&#39;s food nearby, and if I&#39;m hungry, then eat the food)</span>
    <span class="kw">public</span> <span class="kw">class</span> Sequence&lt;T&gt; : BranchNode&lt;T&gt;
    {
        <span class="kw">public</span> <span class="fu">Sequence</span>(<span class="kw">params</span> Node&lt;T&gt;[] children) : <span class="kw">base</span>(children) {}

        <span class="kw">public</span> <span class="kw">override</span> <span class="dt">bool</span> <span class="fu">Update</span>(T context)
        {
            <span class="kw">foreach</span> (var child <span class="kw">in</span> Children)
            {
                <span class="kw">if</span> (!child.<span class="fu">Update</span>(context)) <span class="kw">return</span> <span class="kw">false</span>;
            }
            <span class="kw">return</span> <span class="kw">true</span>;
        }
    }

    <span class="co">// DECORATOR</span>
    <span class="co">// Decorators are nodes that act as a &#39;modifier&#39; for another node</span>
    <span class="co">// This is a base class that just holds a reference to the &#39;modified&#39; or &#39;decorated&#39; node</span>
    <span class="kw">public</span> <span class="kw">abstract</span> <span class="kw">class</span> Decorator&lt;T&gt; : Node&lt;T&gt;
    {
        <span class="kw">protected</span> Node&lt;T&gt; Child { <span class="kw">get</span>; <span class="kw">private</span> <span class="kw">set</span>; }

        <span class="kw">protected</span> <span class="fu">Decorator</span>(Node&lt;T&gt; child)
        {
            Child = child;
        }
    }

    <span class="co">// A common example of a Decorator node is a Not or Negate node</span>
    <span class="co">// that inverts the result of another node. For example if you have a node that checks if there&#39;s any friends</span>
    <span class="co">// nearby you may need to run some logic when there are NO friends nearby. A Not node allows you to do that</span>
    <span class="co">// without having to create a different node</span>
    <span class="kw">public</span> <span class="kw">class</span> Not&lt;T&gt; : Decorator&lt;T&gt;
    {
        <span class="kw">public</span> <span class="fu">Not</span>(Node&lt;T&gt; child) : <span class="kw">base</span>(child) {}

        <span class="kw">public</span> <span class="kw">override</span> <span class="dt">bool</span> <span class="fu">Update</span>(T context)
        {
            <span class="kw">return</span> !Child.<span class="fu">Update</span>(context);
        }
    }

}</code></pre></div>
<h3 id="downsides">Downsides</h3>
<ul>
<li><em>Complicated</em>: BTs can be harder to make sense of than simple FSMs. If your game only has very rudimentary AI that is not likely to evolve much, FSMs might be a better fit.</li>
<li><em>State</em>: Behaviors that need state or have ongoing actions can be hard to represent with BTs (at least the basic version presented here. There are many implementations of BTs that address this problem but they are more complex)</li>
</ul>
<h3 id="footnotes">Footnotes</h3>
<p><b id="f1">1: </b> Technically BTs don't have to be <a href="https://en.wikipedia.org/wiki/Tree_(data_structure)">trees</a> in the computer science sense. More accurately, they are <a href="https://en.wikipedia.org/wiki/Directed_acyclic_graph">directed acyclic graphs</a> or DAGs. The main difference between them is that DAG nodes can have multiple parents (and possible cycles), whereas tree nodes can only have one parent. <a href="#a1">↩</a></p>
<p><b id="f2">2: </b> This aspect of nodes - that they all can be boiled down to an update function that returns the node's status - is what makes BTs so modular. Any node can be plugged into any other since they share this simple, universal interface. <a href="#a2">↩</a></p>
</body>
</html>
